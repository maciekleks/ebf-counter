package main

import (
	"C"
	"bytes"
	"encoding/binary"
	"syscall"

	bpf "github.com/aquasecurity/libbpfgo"
)
import (
	"fmt"
	"os"
	"os/signal"
)

type processInfo struct {
	pid     int
	counter int
}

func main() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	bpfModule, err := bpf.NewModuleFromFile("ebpf-counter.bpf.o")
	must(err)
	defer bpfModule.Close()

	err = bpfModule.BPFLoadObject()
	must(err)

	prog, err := bpfModule.GetProgram("counter_bpftrace")
	must(err)

	_, err = prog.AttachRawTracepoint("sys_enter")
	must(err)

	e := make(chan []byte)
	rb, err := bpfModule.InitRingBuf("events", e)
	must(err)

	rb.Start()

	counter := make(map[string]*processInfo)
loop:
	for {
		select {
		case <-sig:
			fmt.Println("[1]")
			break loop
		case data, ok := <-e:
			if ok == false {
				fmt.Println("[2]")
				break loop
			}
			pid := int(binary.LittleEndian.Uint32(data[0:4])) // Treat first 4 bytes as LittleEndian Uint32
			comm := string(bytes.TrimRight(data[4:], "\x00")) // Remove excess 0's from comm, treat as string
			c, ok := counter[comm]
			if !ok {
				counter[comm] = &processInfo{pid, 1}
				fmt.Printf("new one: [%s]=%v\n", comm, c)
			} else {
				c.counter = c.counter + 1
				fmt.Printf("existing: [%s]=%v\n", comm, c)
			}
		}
	}

	fmt.Println("[3a]")
	rb.Stop()
	fmt.Println("[3b]")
	rb.Stop()
	fmt.Println("[3c]")
	rb.Close()
	fmt.Println("[3d]")
	rb.Close()
	fmt.Println("[3e]")
	rb.Stop()
	fmt.Println("[3f]")
	fmt.Println("[4]")
	for comm, info := range counter {
		fmt.Printf("%s: %v\n", comm, info)
	}

}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
