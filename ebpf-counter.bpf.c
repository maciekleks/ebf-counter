// +build ignore
#include <linux/bpf.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_tracing.h>


char LICENSE[] SEC("license") = "Dual BSD/GPL";

#define TASK_COMM_LEN 100


#ifndef memcpy
#define memcpy(dest, src, n) __builtin_memcpy((dest), (src), (n))
#endif

struct process_info {
	__u32 pid;
	char comm[TASK_COMM_LEN];
};

struct {
    __uint(type, BPF_MAP_TYPE_RINGBUF);
	__uint(max_entries, 1 << 24);
} events SEC(".maps");

long ringbuffer_flags = 0;

SEC("raw_tracepoint/sys_enter")
int counter_bpftrace(void* ctx)
{
    struct process_info pi = {};

	pi.pid = bpf_get_current_pid_tgid() >> 32;
	bpf_get_current_comm(&pi.comm, TASK_COMM_LEN);


	struct process_info *ring_val;
    ring_val = bpf_ringbuf_reserve(&events, sizeof(struct process_info), 0);
    if (!ring_val) {
        return 0;
  	}

  	memcpy(ring_val, &pi, sizeof(struct process_info));


    bpf_ringbuf_submit(ring_val, 0);

    return 0;
}